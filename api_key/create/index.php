<?php
// POST request to http://www.innovations.pe/arturo/api-bweather/api_key/create
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");

// Get database connection
include_once '../../config/Database.php';
include_once '../../objects/ApiKey.php';

$database = new Database();
$db = $database -> getConnection();

$key = new ApiKey($db);

$data = json_decode(file_get_contents("php://input"), true);

$key -> api_key = $key -> key_gen();

if ($data["password"]) {
    if ($data["password"] == "apursh") $key -> type = "permanent";
    else $key -> type = "temporal";
} else $key -> type = "temporal";

$key -> expiration_date = "some_day";

if ($key -> create()) {
    echo json_encode(
        array("message" => "Key added",
            "result" => $key)
    );
} else {
    echo json_encode(
        array("message" => "Unable to add api key")
    );
}