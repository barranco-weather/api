# Barranco Weather's RESTful API
This is the API of Barranco Weather, a weather station project made by students from UTEC.
For more info check https://github.com/arturocuya/barranco-weather

## How to make a request:
The API has two public methods: To add a new measure record and to request data. It's done by a POST method.
This header must be included:
```
Content-Type: application/x-www-form-urlencoded
```

### To add a new measure record:

URL: 
The body of the request must be a raw JSON string like this:
```
{
	"temperature": 24.6,
	"rel_humidity": 70,
	"uv":0.299463,
	"bmp": 0.9895286464812598
}
```

### To request the last recorded measure in JSON:

URL: http://www.innovations.pe/arturo/api-bweather/measures/read_last/
The body of the request must be a raw JSON string like this:
```
{
    "api_key": "AAAAAAAAAAAAAAA"
}
```

### To request all of the recorded measures in JSON:

URL: http://www.innovations.pe/arturo/api-bweather/measures/read_all/
The body of the request must be a raw JSON string like this:
```
{
    "api_key": "AAAAAAAAAAAAAAA"
}
```

The value of "api_key" must be a valid token. To get one visit http://barrancoweather.pythonanywhere.com/ and check the "Datos Libres" section. The "AAAAAAAAAAAAAAA" token is invalid.

## Current measures supported
* Temperature: In Celsius degrees.
* Relative humidity: A percentage that represents, acording to [Wikipedia](https://en.wikipedia.org/wiki/Relative_humidity#Definition), "the ratio of the partial pressure of water vapor in the mixture to the equilibrium vapor pressure of water over a flat surface of pure water at a given temperature"
* UV Intensity: in nanometers
* Pressure: in atm
