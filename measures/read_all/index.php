<?php

// Required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");

// Include database and object files
include_once '../../config/Database.php';
include_once '../../objects/Measures.php';
include_once '../../objects/ApiKey.php';

// Instantiate database and product object
$database = new Database();
$db = $database -> getConnection();

// Initialize product
$measure = new Measures($db);
$key_check = new ApiKey($db);
$data = json_decode(file_get_contents("php://input"), true);
if ($data["api_key"]) {
    if ($key_check -> check_api_key($data["api_key"])) {
        // Query products
        $stmt = $measure -> read_all();
        $num = $stmt -> rowCount();

        // Check if more than 0 records were found
        if ($num > 0) {
            // Products array
            $measure_arr = array();
            $measure_arr["records"] = array();

            // Retrieve table contents
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // Extract row. This will make $row['name'] into just $name
                extract($row);

                $measure_item = array(
                    "temperature" => $temperature,
                    "rel_humidity" => $rel_humidity,
                    "uv" => $uv,
                    "bmp" => $bmp,
                    "created" => $created
                );

                array_push($measure_arr["records"], $measure_item);
            }

            echo json_encode($measure_arr);
        } else {
            echo json_encode(
                array("message" => "No measures found")
            );
        }
    } else {
        echo json_encode(
            array("message" => "Invalid key")
        );
    }
} else {
    echo json_encode(
        array("message" => "No key specified")
    );
}


