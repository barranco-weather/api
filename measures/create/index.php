<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");

// Get database connection
include_once '../../config/Database.php';
include_once '../../objects/Measures.php';

$database = new Database();
$db = $database -> getConnection();

$measure = new Measures($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"), true);

// Set measures values
$measure -> temperature = $data["temperature"];
$measure -> rel_humidity = $data["rel_humidity"];
$measure -> uv = $data["uv"];
$measure -> bmp = $data["bmp"];
echo $data["temperature"] . "\n" . $data["rel_humidity"] . "\n" . $data["uv"] . "\n" . $data["bmp"] . "\n";

if ($measure -> create()) {
    echo json_encode(
        array("message" => "Measures added")
    );
} else {
    echo json_encode(
        array("message" => "Unable to add measure")
    );
}

?>