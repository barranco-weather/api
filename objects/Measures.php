<?php

class Measures {
    /**
     * @var PDO
     */
    private $conn;
    private $table_name = "measures";

    // Object properties
    public $temperature;
    public $rel_humidity;
    public $uv;
    public $bmp;

    // Constructor with $db as database connection
    public function  __construct($db) {
        $this -> conn = $db;
    }

    // Read all measures
    function read_all() {
        $query = "SELECT * FROM " . $this->table_name . " ORDER BY created ASC";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute();
        return $stmt;
    }

    function read_last() {
        $query = "SELECT * FROM " . $this -> table_name . " ORDER BY created DESC LIMIT 1";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute();
        return $stmt;
    }

    function create() {
        $query = "INSERT INTO " . $this -> table_name . " SET temperature=:temperature, rel_humidity=:rel_humidity, uv=:uv, bmp=:bmp, created=CURRENT_TIMESTAMP";

        $stmt = $this -> conn -> prepare($query);

        echo $this -> temperature . "\n" . $this -> rel_humidity . "\n" . $this -> uv . "\n" . $this -> bmp . "\n";

        $stmt -> bindParam(":temperature", $this -> temperature);
        $stmt -> bindParam(":rel_humidity", $this -> rel_humidity);
        $stmt -> bindParam(":uv", $this -> uv);
        $stmt -> bindParam(":bmp",$this -> bmp);

        if ($stmt -> execute()) {
            return true;
        }
        return false;
    }
}

?>