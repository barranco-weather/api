<?php
/**
 * Created by PhpStorm.
 * User: arturocuya
 * Date: 6/10/18
 * Time: 12:50 PM
 */

class ApiKey {
    /**
     * @var PDO
     */
    private $conn;
    private $table_name = "bw_api_keys";

    public $api_key;
    public $type;
    public $created;
    public $status;
    public $expiration_date;

    public function __construct($db) {
        $this -> conn = $db;
    }

    function key_gen() {
        $length = 15;
        $key = '';
        list($usec, $sec) = explode(' ', microtime());
        mt_srand((float) $sec + ((float) $usec * 100000));

        $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));

        for($i=0; $i<$length; $i++)
        {
            $key .= $inputs{mt_rand(0,61)};
        }
        return $key;
    }

    function create() {
        $query = "INSERT INTO ". $this -> table_name . " SET api_key=:api_key, type=:type, created=CURRENT_TIMESTAMP, status=\"active\", expiration_date=:expiration_date";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> bindParam("api_key", $this -> api_key);
        $stmt -> bindParam("type", $this -> type);
        $stmt -> bindParam("expiration_date", $this -> expiration_date);

        if ($stmt -> execute()) {
            return true;
        }
        return false;
    }

    function check_api_key($key) {
        $query = "SELECT * FROM  ". $this -> table_name . " WHERE api_key=:api_key LIMIT 1";
        $stmt = $this -> conn -> prepare($query);

        $stmt -> bindParam("api_key", $key);

        if ($stmt -> execute()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row["api_key"] != "") return true;
        }
        return false;
    }
}